# Groupe
- Boukris
- Berrima

# Lien du jeu de données
https://www.kaggle.com/dgomonov/new-york-city-airbnb-open-data

# Questions proposées
- Dans quelle quartier je peux trouver le meilleur logement?
- C'est quoi le bon prix?
- Caracteristiques de logement parfait pour mes vacances à NYC?

# Question retenue
question [...]

# Remarques éventuelles
[...]
